const tabsCaption = document.querySelectorAll('.tabs-menu li');
for (let i = 0; i < tabsCaption.length; i++) {
    tabsCaption[i].addEventListener('click', function () {
        this.classList.add('active');
        let sibling = this.previousElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.previousElementSibling;
        }
        sibling = this.nextElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.nextElementSibling
        }
        const tabContentId = this.dataset.tabs;

        const tabContent = document.getElementById(tabContentId);
        const tabsContainer = tabContent.parentElement;
        const allTabsContent = tabsContainer.children;
        for (let i = 0; i < allTabsContent.length; i++) {
            allTabsContent[i].classList.remove('active');
        }

        tabContent.classList.add('active');
    })
}
